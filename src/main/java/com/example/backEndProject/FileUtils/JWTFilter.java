package com.example.backEndProject.FileUtils;

import com.auth0.jwt.exceptions.JWTVerificationException;
import com.example.backEndProject.BusinessLogicAsService.JWTUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JWTFilter extends OncePerRequestFilter {

    private JWTUtil jwtUtil;
    private Utils utils;
    private JWTUserDetailService jwtUserDetailService;

    @Autowired
    public JWTFilter(JWTUtil jwtUtil, JWTUserDetailService jwtUserDetailService) {
        this.jwtUtil = jwtUtil;
        this.jwtUserDetailService = jwtUserDetailService;
    }
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
                                    FilterChain filterChain) throws ServletException, IOException
    {
        String header_auth =request.getHeader("Authorization");
        //System.out.println("doFilterInternal invoked :"+header_auth);
        //Check authorization header in bearer token
        if(header_auth !=null && !header_auth.isBlank() && header_auth.startsWith("Bearer "))
        {
            //to get exact token
            String jwt = header_auth.substring(7);
            if(jwt==null || jwt.isBlank())
            {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST,"JWT Token is invalid in Authorization Header");
            }else{
                try {
                    String username = jwtUtil.validateToken(jwt);
                    UserDetails userDetails = jwtUserDetailService.loadUserByUsername(username);
                    UsernamePasswordAuthenticationToken authenticationToken = new
                            UsernamePasswordAuthenticationToken(username, userDetails.getPassword(), userDetails.getAuthorities());
                    //System.out.println("userDetails--------: "+userDetails);
                    //System.out.println("authenticationToken--------: "+authenticationToken);
                    if (SecurityContextHolder.getContext().getAuthentication() == null) {
                        SecurityContextHolder.getContext().setAuthentication(authenticationToken);
                    }
                }catch (JWTVerificationException exception){
                        response.sendError(HttpServletResponse.SC_UNAUTHORIZED,"Invalid Access Token");
                }
            }
        }
        filterChain.doFilter(request,response);
    }
}
