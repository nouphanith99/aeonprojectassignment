package com.example.backEndProject.FileUtils;
import org.springframework.stereotype.Component;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@Component
public class AESEncrypted {
    private String message;
    private byte[] init_vector;
    private SecretKey secretKey;
    private static final String ALGORITHM = "AES/GCM/NoPadding";
    private static final int TAG_LENGTH_BIT = 128;
    private static final int AES_KEY_BIT = 256;

    public SecretKey getSecretKey() {
        return secretKey;
    }
    public void setSecretKey(SecretKey secretKey) {
        this.secretKey = secretKey;
    }
    //secretKey = Utils.getAESKey(AES_KEY_BIT);
    public static byte[] encryption(byte[] text, SecretKey secret, byte[] iv) throws Exception {
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secret, new GCMParameterSpec(TAG_LENGTH_BIT, iv));
        byte[] encryptedText = cipher.doFinal(text);
        return encryptedText;
    }
    public static String encryptWithPrefixIV(byte[] text, SecretKey secret, byte[] vector) throws Exception {
        byte[] cipherText = encryption(text, secret, vector);
        byte[] cipherTextWithIv = ByteBuffer.allocate(vector.length + cipherText.length)
                .put(vector)
                .put(cipherText)
                .array();
        return AESEncrypted.encode(cipherTextWithIv);
    }
    public String dataToBeEncrypted(String json, SecretKey secretKey) throws Exception{
        byte[] init_vector = Utils.getRandomNonce(12);
        String encryptedText = AESEncrypted.encryptWithPrefixIV(json.getBytes(StandardCharsets.UTF_8),secretKey, init_vector);

        //to set secret to use in decrypted class
        //data return to Encrypted (hex)
        //return Utils.hex(encryptedText);
        return encryptedText;
    }
    private static String encode(byte[] data) {
        return Base64.getEncoder().encodeToString(data);
    }
    public String displayHex(byte[] encryptedText) throws Exception {
        return Utils.hex(encryptedText);
    }
}
