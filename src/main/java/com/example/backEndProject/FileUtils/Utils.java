package com.example.backEndProject.FileUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Utils {

    /**
     * sequence is auto generated following date
     * @return sequence number
     */
    public String seqenceGenerated(){
        String seqNo;
        String seqNoGenerated = "yyyyMMddhhmmss";
        String dateTime = "yyyyMMdd";
        SimpleDateFormat seqNoDateFormat = new SimpleDateFormat(seqNoGenerated);
        SimpleDateFormat dateTimeFormat = new SimpleDateFormat(dateTime);
        seqNo = seqNoDateFormat.format(new Date());

        return seqNo;
    }

    /**
     * encrypt and decrypt need the same key.
     *  get AES 256 bits (32 bytes) key
     * @param keysize
     * @return key generated
     * @throws NoSuchAlgorithmException
     */
    // encrypt and decrypt need the same key.
    // get AES 256 bits (32 bytes) key
    // AES secret key
    public static SecretKey getAESKey(int keysize) throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(keysize, SecureRandom.getInstanceStrong());
        return keyGen.generateKey();
    }
    public static byte[] getRandomNonce(int numBytes) {
        byte[] nonce = new byte[numBytes];
        new SecureRandom().nextBytes(nonce);
        return nonce;
    }
    // hex representation
    public static String hex(byte[] bytes) {
        StringBuilder result = new StringBuilder();
        for (byte b : bytes) {
            result.append(String.format("%02x", b));
        }
        return result.toString();
    }

    // print hex with block size split 16
    public static String hexWithBlockSize(byte[] data, int blockSize) {

        String hex = hex(data);
        // one hex = 2 chars
        blockSize = blockSize * 2;
        List<String> result = new ArrayList<>();
        int index = 0;
        while (index < hex.length()) {
            result.add(hex.substring(index, Math.min(index + blockSize, hex.length())));
            index += blockSize;
        }
        return result.toString();
    }
    /**
     * Return  object mapper for data manipulation after decryption with AES
     * @throws Exception
     */
    public List<Class> list(JSONArray data) throws Exception{
        ObjectMapper objectMapper = new ObjectMapper();
        List<Class> response = objectMapper.readValue(String.valueOf(data), List.class);
        return response;
    }
}
