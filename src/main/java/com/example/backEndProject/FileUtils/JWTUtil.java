package com.example.backEndProject.FileUtils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

@Component
public class JWTUtil {

    @Value("${jwt_secret}")
    private String secret;
    //For JWT token generated
    public String tokenGenerated(String username) throws JWTCreationException, IllegalArgumentException
    {
        String access_token = JWT.create()
                .withSubject("Position Authentication")
                .withClaim("username",username)
                .withIssuedAt(new Date())
                .withIssuer("ADMIN")
                .withExpiresAt(new Date(System.currentTimeMillis() + (5 * 60 * 1000))) // 5 minutes
                .sign(Algorithm.HMAC256(secret));

        return access_token;
    }

    //this method is called by JWTFilter to validate JWT Token key
    public String validateToken(String token)throws JWTVerificationException, IOException
    {
        HttpServletResponse response=null;
        String tokenValidation="";

            JWTVerifier jwtVerifier =JWT.require(Algorithm.HMAC256(secret))
                    .withSubject("Position Authentication")
                    .withIssuer("ADMIN")
                    .build();
            DecodedJWT decodedJWT   = jwtVerifier.verify(token);
            tokenValidation  = decodedJWT.getClaim("username").asString();
           // response.sendError(HttpServletResponse.SC_UNAUTHORIZED,"Invalid Access Token");
        return tokenValidation;
    }
}
