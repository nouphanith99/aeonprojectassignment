package com.example.backEndProject.BusinessLogicAsService;

import com.example.backEndProject.Model.StaticUserLogin;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import java.util.Collections;

@Component
public class JWTUserDetailService implements UserDetailsService {

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        StaticUserLogin staticUserLogin = new StaticUserLogin();
        staticUserLogin.setPassword("qwer1234");
        if(username.isBlank() || username==null || username.isEmpty()){
            throw new UsernameNotFoundException("Username must not be empty");
        }
        return new org.springframework.security.core.userdetails.User(
                username,
                staticUserLogin.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_ADMIN")));
    }
}
