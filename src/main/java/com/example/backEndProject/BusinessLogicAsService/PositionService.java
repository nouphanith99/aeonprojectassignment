package com.example.backEndProject.BusinessLogicAsService;

import com.example.backEndProject.Model.Position;
import com.example.backEndProject.RepositioryInterface.PositionJpaRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PositionService {

    @Autowired
    PositionJpaRepo positionJpaRepo;

    public List<Position> getAll(){
        return positionJpaRepo.findAll();
    }
    public Position addPosition(Position position){
        return positionJpaRepo.save(position);
    }
}
