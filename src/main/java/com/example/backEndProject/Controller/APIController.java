package com.example.backEndProject.Controller;

import com.example.backEndProject.BusinessLogicAsService.PositionService;
import com.example.backEndProject.FileUtils.AESDecryption;
import com.example.backEndProject.FileUtils.AESEncrypted;
import com.example.backEndProject.FileUtils.JWTUtil;
import com.example.backEndProject.FileUtils.Utils;
import com.example.backEndProject.Model.Position;
import com.example.backEndProject.Model.StaticUserLogin;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.juli.logging.Log;
import org.apache.juli.logging.LogFactory;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.crypto.SecretKey;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.*;

@Controller
@RequestMapping(path = "/api/v1/")
public class APIController {
    //apache logging
    public final Log logger = LogFactory.getLog(getClass());
    private Utils utils = new Utils();
    private StaticUserLogin staticUserLogin;
    @Autowired
    private AESEncrypted aesEncrypted;
    @Autowired
    private AESDecryption decryption;
    private SecretKey secretKey;
    @Value("${auth_user}")
    private String auth_user;
    @Value("${auth_password}")
    private String auth_password;
    private PositionService positionService;
    private JWTUtil jwtUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public APIController(PositionService positionService, JWTUtil jwtUtil, AuthenticationManager authenticationManager,
                         PasswordEncoder passwordEncoder) {
        this.positionService        = positionService;
        this.jwtUtil                = jwtUtil;
        this.authenticationManager  = authenticationManager;
        this.passwordEncoder        = passwordEncoder;
    }
    /**
     *
     * Public API for all position
     * Created date: 28-July-2022
     * Created by: Nou.Phanith
     */
    @RequestMapping(value = "/list/position",  method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Position>> getPositionInfo() throws Exception
    {
        Gson gson = new GsonBuilder().create();
        List<Position> list_position = new ArrayList<>();
        list_position = positionService.getAll();

        /** ============ Start encryption and decryption with AES ============ */
        //set secret key
        secretKey = Utils.getAESKey(256);
        //convert from array list to json string for encryption
        String json = gson.toJson(list_position);
        String base64Encode = aesEncrypted.dataToBeEncrypted(json,secretKey);
        String data_decode  =  decryption.messageDecode(base64Encode,secretKey);

        System.out.println("AES Base64 Encode: "+base64Encode);
        System.out.println("AES to plain text: "+data_decode);

        JSONArray jsonArray = new JSONArray(data_decode);
        ObjectMapper objectMapper = new ObjectMapper();
        List<Position> json_to_list = objectMapper.readValue(String.valueOf(jsonArray),List.class);
        /** ============ END ============ */

        return new ResponseEntity<>(json_to_list,HttpStatus.OK);
    }
    /**
     *
     * RestAPI: To login using valid user
     * Created date: 28-July-2022
     * Created by: Nou.Phanith
    */
    @RequestMapping(value = "/login", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> loginCredentail(@RequestBody StaticUserLogin user, HttpServletResponse response)
            throws AuthenticationException, IOException {
        try{
            if(!user.getUsername().equals(auth_user) || !user.getPassword().equals(auth_password)) {
                logger.error("Invalide username or password");
                response.sendError(HttpServletResponse.SC_NOT_FOUND,"Invalid User Authentication");
                throw new RuntimeException("Invalid Login Credential");
            }
            UsernamePasswordAuthenticationToken auth_token =
                    new UsernamePasswordAuthenticationToken(user.getUsername(),user.getPassword(), new ArrayList<>());
            //authenticationManager.authenticate(auth_token);
            //get token for valid user credential
            String access_token = jwtUtil.tokenGenerated(user.getUsername());
            return new ResponseEntity<>("TOEKN: "+access_token,HttpStatus.OK);
        }catch (AuthenticationException exception){
            throw new RuntimeException("Invalid Login Credential");
        }
    }
    /**
     *
     * Private API for insert Position
     * Created date: 28-July-2022
     * Created by: Nou.Phanith
     */
    @RequestMapping(value = "/add/position",  method = RequestMethod.POST)
    public ResponseEntity<Position> addPosition(@RequestBody Position position,HttpServletResponse response, HttpServletRequest request) throws Exception {
        try {
            String header_auth =request.getHeader("Authorization");
            if(header_auth==null){
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED,"401 UNAUTHORIZED");
            }else{
                ObjectMapper objectMapper = new ObjectMapper();
                String ID = utils.seqenceGenerated();
                position.setId(ID);
                secretKey = Utils.getAESKey(256);
                Gson gson = new GsonBuilder().create();
                String json = gson.toJson(position);

                String base64Encode = aesEncrypted.dataToBeEncrypted(json,secretKey);
                String data_decode  =  decryption.messageDecode(base64Encode,secretKey);
                JSONParser jsonParser = new JSONParser();

                //String name = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                System.out.println("Encode: "+base64Encode);
                System.out.println("Decode: "+data_decode);
                //return new ResponseEntity<>(positionService.addPosition(position),HttpStatus.OK) ;
                return new ResponseEntity<>(positionService.addPosition(objectMapper.readValue(String.valueOf(jsonParser.parse(data_decode)), Position.class)),HttpStatus.OK) ;
            }
        }catch (Exception e){
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED,"401 UNAUTHORIZED");
            e.printStackTrace();
        }
        return new ResponseEntity<>(position,HttpStatus.OK);
    }

    /**
     *
     * Web back end (Web Admin) to display position information
     * creation date: 30-July-2022
     * Created by: Nou.Phanith
     */
    @RequestMapping(path = "/list/restdisplay",method = RequestMethod.POST)
    public ResponseEntity<List<Position>> rest_displayAllPositionsInfo(Model model) throws Exception
    {
        // Create the request body by wrapping
        // the object in HttpEntity
        HttpEntity<Position> request = new HttpEntity<Position>(new Position());
        RestTemplate restTemplate = new RestTemplate();
        final String position_data_uri = "http://localhost:8181/api/v1/list/position";
        List<Position> positions_list = restTemplate.postForObject(position_data_uri,request,List.class);
        for(int i=0; i<positions_list.size(); i++){
            model.addAttribute("result",positions_list);
        }
        return new ResponseEntity<>(positions_list,HttpStatus.OK);
    }
    @RequestMapping(path = "/list/display",method = RequestMethod.GET)
    public String displayAllPositionsInfo(Model model) throws Exception
    {
        List<Position> positions_list = positionService.getAll();
        for(int i=0; i<positions_list.size(); i++){
            model.addAttribute("result",positions_list);
        }
        return "display";
    }
}
