package com.example.backEndProject.RepositioryInterface;

import com.example.backEndProject.Model.Position;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PositionJpaRepo extends JpaRepository<Position, String> {
    
    List<Position> findAll();
}
