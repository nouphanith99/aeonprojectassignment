package com.example.backEndProject.Model;

import lombok.Data;

@Data
public class StaticUserLogin {
    private String username;
    private String password;
}
