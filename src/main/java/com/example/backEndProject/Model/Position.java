package com.example.backEndProject.Model;

import com.sun.istack.NotNull;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Data
@NoArgsConstructor
@Entity
public class Position {

    @Column(name = "ID", length = 16)
    private String id;

    @Id
    @Column(name = "positionID", length = 8)
    private String positionID;
    @Column(name = "name")
    private String name;
    @Column(name = "description")
    private String description;
}
